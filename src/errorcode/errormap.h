/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ERRORMAP_H
#define ERRORMAP_H

#include <kylin-ai/aiengine.h>

#include <map>

#include "sdkerror.h"

namespace error_map {

inline std::map<ai_engine::lm::NlpEngineErrorCode, sdk_error::NlpError>
    nlpEngineErrorMap{{ai_engine::lm::NlpEngineErrorCode::NomoreMemory,
                       sdk_error::NlpError::NLP_SERVER_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::FileNotFound,
                       sdk_error::NlpError::NLP_SERVER_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::VersionMismatched,
                       sdk_error::NlpError::NLP_SERVER_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::FilePermissionDenied,
                       sdk_error::NlpError::NLP_UNAUTHORIZED},
                      {ai_engine::lm::NlpEngineErrorCode::NetworkDisconnected,
                       sdk_error::NlpError::NLP_NET_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::NetworkLatencyTooLong,
                       sdk_error::NlpError::NLP_NET_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::FailedToConnectServer,
                       sdk_error::NlpError::NLP_SERVER_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::ServerNoResponse,
                       sdk_error::NlpError::NLP_SERVER_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::NoServerService,
                       sdk_error::NlpError::NLP_SERVER_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::Unauthorized,
                       sdk_error::NlpError::NLP_UNAUTHORIZED},
                      {ai_engine::lm::NlpEngineErrorCode::TooLittleParams,
                       sdk_error::NlpError::NLP_INPUT_TEXT_LENGTH_INVAILD},
                      {ai_engine::lm::NlpEngineErrorCode::TooManyParams,
                       sdk_error::NlpError::NLP_INPUT_TEXT_LENGTH_INVAILD},
                      {ai_engine::lm::NlpEngineErrorCode::InvalidParamType,
                       sdk_error::NlpError::NLP_PARAM_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::TooLittleData,
                       sdk_error::NlpError::NLP_INPUT_TEXT_LENGTH_INVAILD},
                      {ai_engine::lm::NlpEngineErrorCode::TooManyData,
                       sdk_error::NlpError::NLP_INPUT_TEXT_LENGTH_INVAILD},
                      {ai_engine::lm::NlpEngineErrorCode::TooFrequentRequest,
                       sdk_error::NlpError::NLP_REQUEST_TOO_MANY},
                      {ai_engine::lm::NlpEngineErrorCode::TooManyRequest,
                       sdk_error::NlpError::NLP_REQUEST_TOO_MANY},
                      {ai_engine::lm::NlpEngineErrorCode::FailedToAnswer,
                       sdk_error::NlpError::NLP_PARAM_ERROR},
                      {ai_engine::lm::NlpEngineErrorCode::InvalidWords,
                       sdk_error::NlpError::NLP_PARAM_ERROR}};

inline std::map<ai_engine::lm::SpeechEngineErrorCode, sdk_error::SpeechError>
    speechEngineErrorMap{
        {ai_engine::lm::SpeechEngineErrorCode::NomoreMemory,
         sdk_error::SpeechError::SPEECH_SERVER_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::FileNotFound,
         sdk_error::SpeechError::SPEECH_SERVER_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::VersionMismatched,
         sdk_error::SpeechError::SPEECH_SERVER_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::FilePermissionDenied,
         sdk_error::SpeechError::SPEECH_UNAUTHORIZED},
        {ai_engine::lm::SpeechEngineErrorCode::NetworkDisconnected,
         sdk_error::SpeechError::SPEECH_NET_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::NetworkLatencyTooLong,
         sdk_error::SpeechError::SPEECH_NET_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::FailedToConnectServer,
         sdk_error::SpeechError::SPEECH_SERVER_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::ServerNoResponse,
         sdk_error::SpeechError::SPEECH_SERVER_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::NoServerService,
         sdk_error::SpeechError::SPEECH_SERVER_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::Unauthorized,
         sdk_error::SpeechError::SPEECH_UNAUTHORIZED},
        {ai_engine::lm::SpeechEngineErrorCode::TooLittleParams,
         sdk_error::SpeechError::SPEECH_TEXT_LENGTH_INVAILD},
        {ai_engine::lm::SpeechEngineErrorCode::TooManyParams,
         sdk_error::SpeechError::SPEECH_TEXT_LENGTH_INVAILD},
        {ai_engine::lm::SpeechEngineErrorCode::InvalidParamType,
         sdk_error::SpeechError::SPEECH_PARAM_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::TooLittleData,
         sdk_error::SpeechError::SPEECH_ADIO_SIZE_INVAILD},
        {ai_engine::lm::SpeechEngineErrorCode::TooManyData,
         sdk_error::SpeechError::SPEECH_ADIO_SIZE_INVAILD},
        {ai_engine::lm::SpeechEngineErrorCode::TooFrequentRequest,
         sdk_error::SpeechError::SPEECH_REQUEST_TOO_MANY},
        {ai_engine::lm::SpeechEngineErrorCode::TooManyRequest,
         sdk_error::SpeechError::SPEECH_REQUEST_TOO_MANY},
        {ai_engine::lm::SpeechEngineErrorCode::UnsupportedLanguage,
         sdk_error::SpeechError::SPEECH_PARAM_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::UnsupportedScript,
         sdk_error::SpeechError::SPEECH_PARAM_ERROR},
        {ai_engine::lm::SpeechEngineErrorCode::InvalidWakeupWords,
         sdk_error::SpeechError::SPEECH_PARAM_ERROR}};

inline std::map<ai_engine::lm::VisionEngineErrorCode, sdk_error::VisoinError>
    visionEngineErrorMap{
        {ai_engine::lm::VisionEngineErrorCode::NomoreMemory,
         sdk_error::VisoinError::VISION_SERVER_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::FileNotFound,
         sdk_error::VisoinError::VISION_SERVER_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::VersionMismatched,
         sdk_error::VisoinError::VISION_SERVER_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::FilePermissionDenied,
         sdk_error::VisoinError::VISION_UNAUTHORIZED},
        {ai_engine::lm::VisionEngineErrorCode::NetworkDisconnected,
         sdk_error::VisoinError::VISION_NET_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::NetworkLatencyTooLong,
         sdk_error::VisoinError::VISION_NET_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::FailedToConnectServer,
         sdk_error::VisoinError::VISION_SERVER_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::ServerNoResponse,
         sdk_error::VisoinError::VISION_SERVER_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::NoServerService,
         sdk_error::VisoinError::VISION_SERVER_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::Unauthorized,
         sdk_error::VisoinError::VISION_UNAUTHORIZED},
        {ai_engine::lm::VisionEngineErrorCode::TooLittleParams,
         sdk_error::VisoinError::VISION_INPUT_TEXT_LENGTH_INVAILD},
        {ai_engine::lm::VisionEngineErrorCode::TooManyParams,
         sdk_error::VisoinError::VISION_INPUT_TEXT_LENGTH_INVAILD},
        {ai_engine::lm::VisionEngineErrorCode::InvalidParamType,
         sdk_error::VisoinError::VISION_PARAM_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::TooLittleData,
         sdk_error::VisoinError::VISION_INPUT_TEXT_LENGTH_INVAILD},
        {ai_engine::lm::VisionEngineErrorCode::TooManyData,
         sdk_error::VisoinError::VISION_INPUT_TEXT_LENGTH_INVAILD},
        {ai_engine::lm::VisionEngineErrorCode::TooFrequentRequest,
         sdk_error::VisoinError::VISION_REQUEST_TOO_MANY},
        {ai_engine::lm::VisionEngineErrorCode::TooManyRequest,
         sdk_error::VisoinError::VISION_REQUEST_TOO_MANY},
        {ai_engine::lm::VisionEngineErrorCode::Invalidwords,
         sdk_error::VisoinError::VISION_PARAM_ERROR},
        {ai_engine::lm::VisionEngineErrorCode::ImageGenerationFailed,
         sdk_error::VisoinError::VISION_GENERATE_IMAGE_FAILED},
        {ai_engine::lm::VisionEngineErrorCode::ImageCensorshipFailed,
         sdk_error::VisoinError::VISION_GENERATE_IMAGE_BLOCKED}};

}  // namespace error_map

#endif  // ERRORMAP_H
