

#ifndef SDKERROR_H
#define SDKERROR_H

namespace sdk_error {

enum class NlpError {
    NLP_SUCCESS = 0,
    NLP_SESSION_ERROR,     // session 错误
    NLP_NET_ERROR,         // 网络错误
    NLP_SERVER_ERROR,      // 服务错误或者不可用
    NLP_UNAUTHORIZED,      // 权限限制，鉴权失败，无权访问
    NLP_REQUEST_TOO_MANY,  // 请求频率过高
    NLP_REQUEST_FAILED,    // 请求失败
    NLP_INPUT_TEXT_LENGTH_INVAILD,  // 输入文本长度超限
    NLP_PARAM_ERROR,                // 参数错误
};

enum class SpeechError {
    SPEECH_SUCCESS = 0,
    SPEECH_SESSION_ERROR,        // session 错误
    SPEECH_NET_ERROR,            // 网络错误
    SPEECH_SERVER_ERROR,         // 服务错误或者不可用
    SPEECH_UNAUTHORIZED,         // 权限限制，鉴权失败，无权访问
    SPEECH_REQUEST_TOO_MANY,     // 请求频率过高
    SPEECH_REQUEST_FAILED,       // 请求失败
    SPEECH_TEXT_LENGTH_INVAILD,  // 输入文本长度超限
    SPEECH_ADIO_SIZE_INVAILD,    // 音频大小超限
    SPEECH_PARAM_ERROR,          // 参数错误
};

enum class VisoinError {
    VISION_SUCCESS = 0,
    VISION_SESSION_ERROR,
    VISION_NET_ERROR,         // 网络错误
    VISION_SERVER_ERROR,      // 服务错误或者不可用
    VISION_UNAUTHORIZED,      // 权限限制，鉴权失败，无权访问
    VISION_REQUEST_TOO_MANY,  // 请求频率过高
    VISION_REQUEST_FAILED,    // 请求失败
    VISION_TASK_TIMEOUT,      // 任务超时
    VISION_INPUT_TEXT_LENGTH_INVAILD,  // 输入文本长度超限
    VISION_GENERATE_IMAGE_FAILED,      // 生成图片失败
    VISION_GENERATE_IMAGE_BLOCKED,     // 图片审核失败
    VISION_PARAM_ERROR,                // 参数错误
};

}  // namespace sdk_error

#endif  // SDKERROR_H
