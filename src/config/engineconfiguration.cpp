/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "engineconfiguration.h"

#include <jsoncpp/json/json.h>

#include <fstream>

namespace config {

const char *promptTemplatesPath = "/etc/kylin-ai/engines/ai-engine";

namespace utils {

Json::Value readJsonValue(const std::string &jsonFile) {
    std::ifstream ifs(jsonFile);
    if (!ifs.is_open()) {
        return Json::Value();
    }
    Json::Value root;
    ifs >> root;
    return root;
}

std::string readJsonMultiKeysStringValue(
    const Json::Value &value, std::initializer_list<std::string> keys) {
    Json::Value currentJsonObj = value;

    for (const auto &key : keys) {
        if (currentJsonObj.isMember(key)) {
            currentJsonObj = currentJsonObj[key];
        }
    }

    return currentJsonObj.isString() ? currentJsonObj.asString() : "";
}

}  // namespace utils

EngineConfiguration::EngineConfiguration() {
    loadAiEngineConfig();
    loadDbEngineConfig();
}

std::string config::EngineConfiguration::currentLmSpeechEngineName() const {
    return utils::readJsonMultiKeysStringValue(
        aiEngineConfig_, {"large-model", "current", "speech"});
}

std::string EngineConfiguration::currentLmNlpEngineName() const {
    return utils::readJsonMultiKeysStringValue(
        aiEngineConfig_, {"large-model", "current", "nlp"});
}

std::string EngineConfiguration::currentLmVisionEngineName() const {
    return utils::readJsonMultiKeysStringValue(
        aiEngineConfig_, {"large-model", "current", "vision"});
}

std::string EngineConfiguration::currentTmOcrEngineName() const {
    return utils::readJsonMultiKeysStringValue(
        aiEngineConfig_, {"traditional-model", "current", "ocr"});
}

std::string EngineConfiguration::currentVdEngineName() const {
    return utils::readJsonMultiKeysStringValue(
        dbEngineConfig_, {"traditional-model", "current", "ocr"});
}

std::string EngineConfiguration::promptTemplateConfigFile() const {
    return promptTemplatesPath + "/" + currentLmNlpEngineName() +
           "/prompt.json";
}

std::string EngineConfiguration::promptFilePath() const {
    return promptTemplatesPath + "/" + currentLmNlpEngineName() + "/prompts";
}

void EngineConfiguration::loadAiEngineConfig() {
    aiEngineConfig_ = utils::readJsonValue(aiEngineConfigFile_);
}

void EngineConfiguration::loadDbEngineConfig() {
    dbEngineConfig_ = utils::readJsonValue(dbEngineConfigFile_);
}

const std::string promptTemplateConfigFile(const std::string &engineName) {
    return std::string(promptTemplatesPath) + "/" + engineName + "/prompt.json";
}

const std::string promptFilePath(const std::string &engineName) {
    return std::string(promptTemplatesPath) + "/" + engineName + "/prompts";
}

}  // namespace config
