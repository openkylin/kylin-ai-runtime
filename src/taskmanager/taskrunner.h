#ifndef TASKRUNNER_H
#define TASKRUNNER_H

#include <atomic>
#include <cstdio>
#include <functional>
#include <mutex>
#include <set>
#include <thread>
#include <unordered_map>

#include "concurrentqueue/concurrentqueue.h"

namespace task {

ulong generateTaskId();

enum class TaskType {
    TextChat,
    TtsOnce,
    TtsContinuous,
    AsrOnce,
    AsrContinuous,
    Text2Image,
    Initialization,
    Stop,
};

class Task {
public:
    Task() = default;
    Task(ulong taskId, TaskType taskType, std::function<void()> task);
    void operator()() const;

public:
    std::function<void()> task;
    ulong taskId;
    TaskType taskType;
};

class TaskRunner {
public:
    static TaskRunner &getInstance();
    ~TaskRunner();

    void addTask(Task task);
    void deleteTask(ulong taskId);
    void waitForTasks();
    void setMaxConcurrentTasks(TaskType taskType, int maxTasks);

private:
    TaskRunner();

    void dispatchTasks();
    void dispatchTask(Task &task);
    void handleWaittingTask(TaskType taskType);

    bool isCanceledTask(ulong taskId);
    void removeCanceledTask(ulong taskId);

private:
    std::thread *dispatchThread{nullptr};
    moodycamel::ConcurrentQueue<Task> taskQueue;
    std::set<ulong> canceledTasks;
    std::unordered_map<TaskType, moodycamel::ConcurrentQueue<Task>>
        waittingQueue;
    std::atomic<bool> stop{false};
    std::unordered_map<TaskType, int> maxConcurrentTasks = {
        {TaskType::TextChat, 8},      {TaskType::TtsOnce, 2},
        {TaskType::TtsContinuous, 4}, {TaskType::AsrOnce, 4},
        {TaskType::AsrContinuous, 4}, {TaskType::Text2Image, 4},
        {TaskType::Initialization, 4}};
    std::unordered_map<TaskType, std::atomic_int> currentTasks;
    std::mutex mutex_;
};

}  // namespace task

#endif  // TASKRUNNER_H
