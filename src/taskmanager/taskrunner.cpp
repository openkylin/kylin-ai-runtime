#include "taskrunner.h"

#include <sys/types.h>

#include <mutex>
#include <thread>

#include "threadpool/async.h"

namespace task {

ulong generateTaskId() {
    static std::atomic_ulong taskId = 0;
    if (taskId == std::numeric_limits<std::atomic_ulong>::max()) {
        taskId = 1;
    } else {
        taskId++;
    }
    return taskId;
}

Task::Task(ulong taskId, TaskType taskType, std::function<void()> task)
    : taskId(taskId), taskType(taskType), task(task) {}

void Task::operator()() const { task(); }

TaskRunner &TaskRunner::getInstance() {
    static TaskRunner taskRunner;
    return taskRunner;
}

TaskRunner::TaskRunner() {
    dispatchThread = new std::thread(&TaskRunner::dispatchTasks, this);
    cpr::async::startup(8);
}

TaskRunner::~TaskRunner() {
    stop = true;
    if (dispatchThread != nullptr) {
        dispatchThread->join();
        delete dispatchThread;
    }
}

void TaskRunner::addTask(Task task) { taskQueue.enqueue(std::move(task)); }

void TaskRunner::deleteTask(ulong taskId) {
    std::lock_guard<std::mutex> locker(mutex_);
    canceledTasks.insert(taskId);
}

void TaskRunner::waitForTasks() {
    while (taskQueue.size_approx() > 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
}

void TaskRunner::setMaxConcurrentTasks(TaskType taskType, int maxTasks) {
    maxConcurrentTasks[taskType] = maxTasks;
}

void TaskRunner::dispatchTasks() {
    while (!stop) {
        Task task;
        if (taskQueue.try_dequeue(task)) {
            if (!isCanceledTask(task.taskId)) {
                dispatchTask(task);
            } else {
                removeCanceledTask(task.taskId);
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
}

void TaskRunner::dispatchTask(Task &task) {
    if (currentTasks[task.taskType] < maxConcurrentTasks[task.taskType]) {
        cpr::async([this, task]() {
            currentTasks[task.taskType]++;
            task();
            handleWaittingTask(task.taskType);
            currentTasks[task.taskType]--;
        });
    } else {
        waittingQueue[task.taskType].enqueue(task);
    }
}

void TaskRunner::handleWaittingTask(TaskType taskType) {
    Task task;
    while (waittingQueue[taskType].size_approx() != 0) {
        if (waittingQueue[taskType].try_dequeue(task)) {
            task();
        }
    }
}

bool TaskRunner::isCanceledTask(ulong taskId) {
    std::lock_guard<std::mutex> locker(mutex_);
    return canceledTasks.find(taskId) != canceledTasks.end();
}

void TaskRunner::removeCanceledTask(ulong taskId) {
    std::lock_guard<std::mutex> locker(mutex_);
    canceledTasks.erase(taskId);
}

};  // namespace task
