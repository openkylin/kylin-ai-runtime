/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "speechprocessor.h"

#include <kylin-ai/aiengine.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

#include <cstdio>
#include <thread>

#include "errorcode/errormap.h"
#include "taskmanager/taskrunner.h"

const std::string SpeechProcessor::objectPath_ =
    "/org/openkylin/aisdk/speechprocessor";
std::set<int> SpeechProcessor::sessionIdSet_;

SpeechProcessor::SpeechProcessor(
    GDBusConnection &connection,
    ai_engine::AiEnginePluginManager &aiEnginePluginManager)
    : connection_(connection), aiEnginePluginManager_(aiEnginePluginManager) {
    exportSkeleton();
}

SpeechProcessor::~SpeechProcessor() { unexportSkeleton(); }

void SpeechProcessor::stopProcess() {
    SpeechEngineMap::iterator iter = speechEngineMap_.begin();
    while (iter != speechEngineMap_.end()) {
        ai_engine::lm::EngineError stopSynthesisEngineError;
        if (!iter->second->stopContinuousSynthesis(stopSynthesisEngineError)) {
            printEngineErrorMessages(stopSynthesisEngineError);
        }

        ai_engine::lm::EngineError stopRecognitionEngineError;
        if (!iter->second->stopContinuousRecognition(
                stopRecognitionEngineError)) {
            printEngineErrorMessages(stopRecognitionEngineError);
        }
        ++iter;
    }
    stopped_ = true;
}

void SpeechProcessor::exportSkeleton() {
    delegate_ = aisdk_speech_processor_skeleton_new();
    connectSignal();

    GError *error = nullptr;

    isExported_ = g_dbus_interface_skeleton_export(
        G_DBUS_INTERFACE_SKELETON(delegate_), &connection_, objectPath_.c_str(),
        &error);
    if (!isExported_) {
        g_printerr("Error creating server at address %s: %s\n",
                   objectPath_.c_str(), error->message);
        g_error_free(error);
    }
}

std::string SpeechProcessor::base64decode(const std::string &input,
                                          const int dataLength) {
    BIO *bio, *b64;
    char *buffer = new char[dataLength];
    memset(buffer, 0, dataLength);

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_new_mem_buf(input.c_str(), dataLength);
    bio = BIO_push(b64, bio);

    BIO_read(bio, buffer, dataLength);

    BIO_free_all(bio);

    std::string result(buffer, buffer + dataLength);

    delete[] buffer;
    return result;
}

void SpeechProcessor::emitRecognizingResult(
    ai_engine::lm::speech::RecognitionResult result, const gchar *interfaceName,
    const gchar *signalName) {
    GVariantBuilder builder;
    g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);
    g_variant_builder_add(&builder, "s", result.text.c_str());
    g_variant_builder_add(&builder, "i", result.text.length());
    g_variant_builder_add(&builder, "i", result.speakerId);
    g_variant_builder_add(&builder, "i",
                          getSdkErrorCode(result.error.getCode()));

    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(this)->getSharedObject();

    g_dbus_connection_emit_signal(
        &(speechProcessor->connection_), "org.openkylin.aisdk.speechprocessor",
        "/org/openkylin/aisdk/speechprocessor", interfaceName, signalName,
        g_variant_builder_end(&builder), nullptr);
}

void SpeechProcessor::emitSynthesizingResult(
    ai_engine::lm::speech::SynthesisResult result, const gchar *interfaceName,
    const gchar *signalName) {
    // 创建包含字节数组的 GVariant
    GVariant *byte_array_variant = g_variant_new_from_data(
        G_VARIANT_TYPE_BYTESTRING, result.audioData.data(),
        result.audioData.size(), TRUE, NULL, NULL);

    GVariantBuilder builder;
    g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);
    g_variant_builder_open(&builder, G_VARIANT_TYPE_ARRAY);
    g_variant_builder_add_value(&builder, byte_array_variant);
    g_variant_builder_close(&builder);

    g_variant_builder_add(&builder, "s", result.audioFormat.c_str());
    g_variant_builder_add(&builder, "i", result.audioRate);
    g_variant_builder_add(&builder, "i", result.audioChannel);
    g_variant_builder_add(&builder, "i",
                          getSdkErrorCode(result.error.getCode()));

    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(this)->getSharedObject();

    g_dbus_connection_emit_signal(
        &(speechProcessor->connection_), "org.openkylin.aisdk.speechprocessor",
        "/org/openkylin/aisdk/speechprocessor", interfaceName, signalName,
        g_variant_builder_end(&builder), nullptr);
}

void SpeechProcessor::emitStopSynthesizingAudioSignal(
    const gchar *interfaceName) {
    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(this)->getSharedObject();

    g_dbus_connection_emit_signal(
        &(speechProcessor->connection_), "org.openkylin.aisdk.speechprocessor",
        "/org/openkylin/aisdk/speechprocessor", interfaceName,
        "StopSynthesizingAudioResult", nullptr, nullptr);
}

bool SpeechProcessor::initEngine(const std::string &engineName,
                                 const std::string &config, gint sessionId) {
    auto speechEngine = aiEnginePluginManager_.createSpeechEngine(engineName);
    if (!speechEngine) {
        fprintf(
            stderr,
            "service error messages: initEngine speechEngine is nullptr!\n");
        return false;
    }
    speechEngineMap_[sessionId] = std::move(speechEngine);
    if (speechEngineMap_.at(sessionId)->isCloud()) {
        static_cast<ai_engine::lm::speech::AbstractCloudSpeechEngine *>(
            speechEngineMap_.at(sessionId).get())
            ->setConfig(config);
    }

    setMaxConcurrentTasks(sessionId);

    return true;
}

void SpeechProcessor::setMaxConcurrentTasks(int sessionId) {
    if (speechEngineMap_.find(sessionId) == speechEngineMap_.end()) {
        return;
    }

    task::TaskRunner::getInstance().setMaxConcurrentTasks(
        task::TaskType::AsrContinuous,
        speechEngineMap_.at(sessionId)
            ->maxConcurrentContinuousRecognitionTasks());
    task::TaskRunner::getInstance().setMaxConcurrentTasks(
        task::TaskType::AsrOnce,
        speechEngineMap_.at(sessionId)->maxConcurrentOnceRecognitionTasks());
    task::TaskRunner::getInstance().setMaxConcurrentTasks(
        task::TaskType::TtsContinuous,
        speechEngineMap_.at(sessionId)
            ->maxConcurrentContinuousSynthesisTasks());
    task::TaskRunner::getInstance().setMaxConcurrentTasks(
        task::TaskType::TtsOnce,
        speechEngineMap_.at(sessionId)->maxConcurrentOnceSynthesisTasks());
}

int SpeechProcessor::initSessionModule(gint sessionId) {
    ai_engine::lm::EngineError initModuleEngineError;

    ai_engine::lm::EngineError initContinuousRecognitionModuleEngineError;
    continuousRecognitionInited_ =
        speechEngineMap_.at(sessionId)->initContinuousRecognitionModule(
            initContinuousRecognitionModuleEngineError);
    if (!continuousRecognitionInited_) {
        fprintf(stderr, "init continuous recognition module engine error!\n");
        initModuleEngineError = initContinuousRecognitionModuleEngineError;
    }

    ai_engine::lm::EngineError initRecognizeOnceModuleEngineError;
    onceRecognitionInited_ =
        speechEngineMap_.at(sessionId)->initRecognizeOnceModule(
            initRecognizeOnceModuleEngineError);
    if (!onceRecognitionInited_) {
        fprintf(stderr, "init recognition once module engine error!\n");
        initModuleEngineError = initRecognizeOnceModuleEngineError;
    }

    ai_engine::lm::EngineError initContinuousSynthesisModuleEngineError;
    continuousSynthesisInited_ =
        speechEngineMap_.at(sessionId)->initContinuousSynthesisModule(
            initContinuousSynthesisModuleEngineError);
    if (!continuousSynthesisInited_) {
        fprintf(stderr, "init continuous synthesize module engine error!\n");
        initModuleEngineError = initContinuousSynthesisModuleEngineError;
    }

    ai_engine::lm::EngineError initSynthesizeOnceModuleEngineError;
    onceSynthesisInited_ =
        speechEngineMap_.at(sessionId)->initSynthesizeOnceModule(
            initSynthesizeOnceModuleEngineError);
    if (!onceSynthesisInited_) {
        fprintf(stderr, "init synthesize once module engine error!\n");
        initModuleEngineError = initSynthesizeOnceModuleEngineError;
    }

    if (continuousRecognitionInited_ || onceRecognitionInited_ ||
        continuousSynthesisInited_ || onceSynthesisInited_) {
        return 0;
    }
    return initModuleEngineError.getCode();
}

void SpeechProcessor::setSessionRecognitionCallback(gint sessionId) {
    std::string interfaceName =
        "org.openkylin.aisdk.speechprocessor" + std::to_string((int)sessionId);

    speechEngineMap_.at(sessionId)->setRecognizingCallback(
        [this, interfaceName](ai_engine::lm::speech::RecognitionResult result) {
            if (stopped_) { return; }
            emitRecognizingResult(result, interfaceName.c_str(),
                                  "RecognizingAudioResult");
        });

    speechEngineMap_.at(sessionId)->setRecognizedCallback(
        [this, interfaceName](ai_engine::lm::speech::RecognitionResult result) {
            if (stopped_) { return; }
            emitRecognizingResult(result, interfaceName.c_str(),
                                  "StopRecognizingAudioResult");
        });
}

void SpeechProcessor::setSessionSynthesisCallback(gint sessionId) {
    std::string interfaceName =
        "org.openkylin.aisdk.speechprocessor" + std::to_string((int)sessionId);

    speechEngineMap_.at(sessionId)->setSynthesizingCallback(
        [this, interfaceName](ai_engine::lm::speech::SynthesisResult result) {
            if (stopped_) { return; }
            fprintf(stdout, "interfaceName: %s\n", interfaceName.c_str());
            fprintf(stdout, "synthesis result size: %lu\n",
                    result.audioData.size());
            emitSynthesizingResult(result, interfaceName.c_str(),
                                   "SynthesizingAudioResult");
        });

    speechEngineMap_.at(sessionId)->setSynthesizedCallback(
        [this, interfaceName](ai_engine::lm::speech::ResultType result) {
            fprintf(stdout, "interfaceName: %s\n", interfaceName.c_str());

            emitStopSynthesizingAudioSignal(interfaceName.c_str());
        });
}

bool SpeechProcessor::onHandleInitEngine(AisdkSpeechProcessor *delegate,
                                         GDBusMethodInvocation *invocation,
                                         gchar *engineName, gchar *config,
                                         gpointer userData) {
    auto speechProcessor =
        static_cast<SpeechProcessor *>(userData)->getSharedObject();
    if (!speechProcessor) {
        fprintf(stderr, "init engine error: speechProcessor is nullptr!\n");
        aisdk_speech_processor_complete_init(
            delegate, invocation, 0,
            (int)error_map::speechEngineErrorMap.at(
                ai_engine::lm::SpeechEngineErrorCode::NomoreMemory));
        return true;
    }

    int sessionId = speechProcessor->generateRandomSessionId();
    std::string engineNameStr{engineName};
    std::string configStr{config};

    task::TaskRunner::getInstance().addTask(
        {task::generateTaskId(), task::TaskType::Initialization,
         [delegate, invocation, speechProcessor, sessionId, engineNameStr,
          configStr]() {
             bool initResult = speechProcessor->initEngine(
                 engineNameStr, configStr, sessionId);
             if (!initResult) {
                 fprintf(stderr, "engine initialization failed!\n");
                 aisdk_speech_processor_complete_init(
                     delegate, invocation, sessionId,
                     (int)error_map::speechEngineErrorMap.at(
                         ai_engine::lm::SpeechEngineErrorCode::NomoreMemory));
                 return true;
             }

             int initModuleErrorCode =
                 speechProcessor->initSessionModule(sessionId);

             fprintf(stdout, "the sessionId of the engine is %d\n", sessionId);
             speechProcessor->setSessionRecognitionCallback(sessionId);
             speechProcessor->setSessionSynthesisCallback(sessionId);
             aisdk_speech_processor_complete_init(
                 delegate, invocation, sessionId, initModuleErrorCode);
             return true;
         }});

    return true;
}

bool SpeechProcessor::onHandleDestroyEngine(AisdkSpeechProcessor *delegate,
                                            GDBusMethodInvocation *invocation,
                                            gint sessionId, gpointer userData) {
    auto *speechProcessor = static_cast<SpeechProcessor *>(userData);
    if (!speechProcessor) {
        fprintf(stderr,
                "speech destroy session error: speechProcessor is nullptr!\n");
        return true;
    }
    auto iter = speechProcessor->speechEngineMap_.find(sessionId);
    if (iter == speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "speech destroy session error: sessionId not exists!\n");
        return true;
    }

    ai_engine::lm::EngineError stopSynthesisEngineError;
    if (!iter->second->stopContinuousSynthesis(stopSynthesisEngineError)) {
        speechProcessor->printEngineErrorMessages(stopSynthesisEngineError);
    }

    ai_engine::lm::EngineError stopRecognitionEngineError;
    if (!iter->second->stopContinuousRecognition(stopRecognitionEngineError)) {
        speechProcessor->printEngineErrorMessages(stopRecognitionEngineError);
    }
    speechProcessor->speechEngineMap_.erase(iter);
    return true;
}

bool SpeechProcessor::onHandleStartContinuousRecognition(
    AisdkSpeechProcessor *delegate, GDBusMethodInvocation *invocation,
    gchar *audioProperties, gint sessionId, gpointer userData) {
    auto *speechProcessor = static_cast<SpeechProcessor *>(userData);
    if (!speechProcessor || speechProcessor->speechEngineMap_.find(sessionId) ==
                                speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "start recognize error: speechProcessor or speechEngine is "
                "nullptr!\n");
        aisdk_speech_processor_complete_start_continuous_recognition(
            delegate, invocation,
            (int)error_map::speechEngineErrorMap.at(
                ai_engine::lm::SpeechEngineErrorCode::NomoreMemory));
        return true;
    }

    ai_engine::lm::EngineError engineError;
    bool success =
        speechProcessor->speechEngineMap_.at(sessionId)
            ->startContinuousRecognition(audioProperties, engineError);
    if (!success) {
        speechProcessor->printEngineErrorMessages(engineError);
        aisdk_speech_processor_complete_start_continuous_recognition(
            delegate, invocation,
            (int)error_map::speechEngineErrorMap.at(
                ai_engine::lm::SpeechEngineErrorCode(engineError.getCode())));
        return true;
    }

    speechProcessor->continuousStop_ = false;
    task::TaskRunner::getInstance().addTask(
        {task::generateTaskId(), task::TaskType::TtsContinuous,
         [speechProcessor, sessionId]() {
             while (!speechProcessor->continuousStop_) {
                 std::string data;
                 if (speechProcessor->audioDataQueue_.try_dequeue(data)) {
                     std::string decodeData =
                         speechProcessor->base64decode(data, data.size());
                     std::vector<char> inputData{
                         decodeData.data(),
                         decodeData.data() + decodeData.size()};
                     ai_engine::lm::EngineError engineError;
                     bool success =
                         speechProcessor->speechEngineMap_.at(sessionId)
                             ->writeContinuousRecognitionAudioData(inputData,
                                                                   engineError);
                     if (!success) {
                         speechProcessor->printEngineErrorMessages(engineError);
                     }
                 }
                 std::this_thread::sleep_for(std::chrono::milliseconds(10));
             }
         }});

    aisdk_speech_processor_complete_start_continuous_recognition(delegate,
                                                                 invocation, 0);
    return true;
}

bool SpeechProcessor::onHandleWriteContinuousAudioData(
    AisdkSpeechProcessor *delegate, GDBusMethodInvocation *invocation,
    gchar *audioData, gint audioDataLength, gint sessionId, gpointer userData) {
    // audioData是编码后的数据
    // audioDataLength是原始数据的数据长度
    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(userData)->getSharedObject();
    if (!speechProcessor || speechProcessor->speechEngineMap_.find(sessionId) ==
                                speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "write continue audio data error: speechProcessor or "
                "speechEngine is nullptr!\n");
        return true;
    }

    std::string data{audioData};
    speechProcessor->audioDataQueue_.enqueue(data);
    return true;
}

bool SpeechProcessor::onHandleStopContinuousRecognition(
    AisdkSpeechProcessor *delegate, GDBusMethodInvocation *invocation,
    gint sessionId, gpointer userData) {
    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(userData)->getSharedObject();
    if (!speechProcessor || speechProcessor->speechEngineMap_.find(sessionId) ==
                                speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "stop recognizing error: speechProcessor or speechEngine is "
                "nullptr!\n");
        return true;
    }

    speechProcessor->continuousStop_ = true;
    ai_engine::lm::EngineError engineError;
    bool success = speechProcessor->speechEngineMap_.at(sessionId)
                       ->stopContinuousRecognition(engineError);
    if (!success) {
        speechProcessor->printEngineErrorMessages(engineError);
    }

    return true;
}

bool SpeechProcessor::onHandleSpeechRecognizeOnce(
    AisdkSpeechProcessor *delegate, GDBusMethodInvocation *invocation,
    gchar *audioProperties, gchar *audioData, gint audioDataLength,
    gint sessionId, gpointer userData) {
    // audioData是编码后的数据
    // audioDataLength是原始数据的数据长度
    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(userData)->getSharedObject();
    if (!speechProcessor || speechProcessor->speechEngineMap_.find(sessionId) ==
                                speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "recognizing complete audio once error: speechProcessor or "
                "speechEngine is nullptr!\n");
        return true;
    }

    std::string data{audioData};
    std::string params{audioProperties};
    task::TaskRunner::getInstance().addTask(
        {task::generateTaskId(), task::TaskType::TtsOnce,
         [params, audioDataLength, speechProcessor, sessionId, data]() {
             std::string decodeData =
                 speechProcessor->base64decode(data, audioDataLength);
             std::vector<char> inputData{decodeData.data(),
                                         decodeData.data() + decodeData.size()};
             ai_engine::lm::EngineError engineError;
             bool success =
                 speechProcessor->speechEngineMap_.at(sessionId)->recognizeOnce(
                     params, inputData, engineError);
             if (!success) {
                 speechProcessor->printEngineErrorMessages(engineError);
             }
         }});

    return true;
}

bool SpeechProcessor::onHandleStartContinuousSynthesis(
    AisdkSpeechProcessor *delegate, GDBusMethodInvocation *invocation,
    gchar *voiceProperties, gint sessionId, gpointer userData) {
    auto *speechProcessor = static_cast<SpeechProcessor *>(userData);
    if (!speechProcessor || speechProcessor->speechEngineMap_.find(sessionId) ==
                                speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "start continuous synthesis error: speechProcessor or "
                "speechEngine is nullptr!\n");
        aisdk_speech_processor_complete_start_continuous_synthesis(
            delegate, invocation,
            (int)error_map::speechEngineErrorMap.at(
                ai_engine::lm::SpeechEngineErrorCode::NomoreMemory));
        return true;
    }

    ai_engine::lm::EngineError engineError;
    bool success = speechProcessor->speechEngineMap_.at(sessionId)
                       ->startContinuousSynthesis(voiceProperties, engineError);
    if (!success) {
        speechProcessor->printEngineErrorMessages(engineError);
        aisdk_speech_processor_complete_start_continuous_synthesis(
            delegate, invocation,
            (int)error_map::speechEngineErrorMap.at(
                ai_engine::lm::SpeechEngineErrorCode(engineError.getCode())));
        return true;
    }

    speechProcessor->continuousStop_ = false;
    task::TaskRunner::getInstance().addTask(
        {task::generateTaskId(), task::TaskType::AsrOnce,
         [speechProcessor, sessionId]() {
             while (!speechProcessor->continuousStop_) {
                 std::string data;
                 if (speechProcessor->textDataQueue_.try_dequeue(data)) {
                     ai_engine::lm::EngineError engineError;
                     bool success =
                         speechProcessor->speechEngineMap_.at(sessionId)
                             ->writeContinuousSynthesisText(data, engineError);
                     if (!success) {
                         speechProcessor->printEngineErrorMessages(engineError);
                     }
                 }
                 std::this_thread::sleep_for(std::chrono::milliseconds(10));
             }
         }});

    aisdk_speech_processor_complete_start_continuous_synthesis(delegate,
                                                               invocation, 0);
    return true;
}

bool SpeechProcessor::onHandleWriteContinuousText(
    AisdkSpeechProcessor *delegate, GDBusMethodInvocation *invocation,
    gchar *text, gint sessionId, gpointer userData) {
    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(userData)->getSharedObject();
    if (!speechProcessor || speechProcessor->speechEngineMap_.find(sessionId) ==
                                speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "write continuous synthesis text error: speechProcessor or "
                "speechEngine is nullptr!\n");
        return true;
    }

    std::string textString{text};
    speechProcessor->textDataQueue_.enqueue(textString);

    return true;
}

bool SpeechProcessor::onHandleStopContinuousSynthesis(
    AisdkSpeechProcessor *delegate, GDBusMethodInvocation *invocation,
    gint sessionId, gpointer userData) {
    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(userData)->getSharedObject();
    if (!speechProcessor || speechProcessor->speechEngineMap_.find(sessionId) ==
                                speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "stop continuous synthesis error: speechProcessor or "
                "speechEngine is nullptr!\n");
        return true;
    }

    ai_engine::lm::EngineError engineError;
    speechProcessor->continuousStop_ = true;
    bool success = speechProcessor->speechEngineMap_.at(sessionId)
                       ->stopContinuousSynthesis(engineError);
    if (!success) {
        speechProcessor->printEngineErrorMessages(engineError);
    }
    return true;
}

bool SpeechProcessor::onHandleSpeechSynthesizeOnce(
    AisdkSpeechProcessor *delegate, GDBusMethodInvocation *invocation,
    gchar *voiceProperties, gchar *text, gint sessionId, gpointer userData) {
    std::shared_ptr<SpeechProcessor> speechProcessor =
        static_cast<SpeechProcessor *>(userData)->getSharedObject();
    if (!speechProcessor || speechProcessor->speechEngineMap_.find(sessionId) ==
                                speechProcessor->speechEngineMap_.end()) {
        fprintf(stderr,
                "synthesize once error: speechProcessor or speechEngine is "
                "nullptr!\n");
        return true;
    }

    std::string textString{text};
    std::string propertiesString{voiceProperties};
    task::TaskRunner::getInstance().addTask(
        {task::generateTaskId(), task::TaskType::AsrOnce,
         [speechProcessor, sessionId, textString, propertiesString]() {
             ai_engine::lm::EngineError engineError;
             bool success = speechProcessor->speechEngineMap_.at(sessionId)
                                ->synthesizeOnce(propertiesString, textString,
                                                 engineError);
             if (!success) {
                 speechProcessor->printEngineErrorMessages(engineError);
             }
         }});

    return true;
}

void SpeechProcessor::connectSignal() {
    g_signal_connect(delegate_, "handle-init", G_CALLBACK(onHandleInitEngine),
                     this);
    g_signal_connect(delegate_, "handle-destroy",
                     G_CALLBACK(onHandleDestroyEngine), this);

    g_signal_connect(delegate_, "handle-start-continuous-recognition",
                     G_CALLBACK(onHandleStartContinuousRecognition), this);
    g_signal_connect(delegate_, "handle-write-continuous-audio-data",
                     G_CALLBACK(onHandleWriteContinuousAudioData), this);
    g_signal_connect(delegate_, "handle-stop-continuous-recognition",
                     G_CALLBACK(onHandleStopContinuousRecognition), this);
    g_signal_connect(delegate_, "handle-speech-recognize-once",
                     G_CALLBACK(onHandleSpeechRecognizeOnce), this);

    g_signal_connect(delegate_, "handle-start-continuous-synthesis",
                     G_CALLBACK(onHandleStartContinuousSynthesis), this);
    g_signal_connect(delegate_, "handle-write-continuous-text",
                     G_CALLBACK(onHandleWriteContinuousText), this);
    g_signal_connect(delegate_, "handle-stop-continuous-synthesis",
                     G_CALLBACK(onHandleStopContinuousSynthesis), this);
    g_signal_connect(delegate_, "handle-speech-synthesize-once",
                     G_CALLBACK(onHandleSpeechSynthesizeOnce), this);
}

void SpeechProcessor::unexportSkeleton() {
    if (delegate_ == nullptr) {
        return;
    }

    if (isExported_) {
        g_dbus_interface_skeleton_unexport_from_connection(
            G_DBUS_INTERFACE_SKELETON(delegate_), &connection_);
    }
    sessionIdSet_.clear();
    g_object_unref(delegate_);
}

int SpeechProcessor::generateRandomSessionId() {
    int sessionId = std::rand();
    while (sessionIdSet_.find(sessionId) != sessionIdSet_.end()) {
        sessionId = std::rand();
    }
    sessionIdSet_.insert(sessionId);
    return sessionId;
}

void SpeechProcessor::printEngineErrorMessages(
    const ai_engine::lm::EngineError &engineError) {
    fprintf(stderr, "service error messages: %s!\n",
            engineError.getMessage().c_str());
}

int SpeechProcessor::getSdkErrorCode(int engineErrorCode) {
    if (engineErrorCode == -1) {
        return 0;
    } else if (engineErrorCodeExists(engineErrorCode)) {
        return (int)error_map::speechEngineErrorMap.at(
            (ai_engine::lm::SpeechEngineErrorCode)engineErrorCode);
    } else {
        fprintf(
            stderr,
            "get sdk code error: speech engine error code is not exists!\n");
        return 0;
    }
}

bool SpeechProcessor::engineErrorCodeExists(int errorCode) {
    ai_engine::lm::SpeechEngineErrorCode code =
        (ai_engine::lm::SpeechEngineErrorCode)errorCode;
    return error_map::speechEngineErrorMap.find(code) !=
           error_map::speechEngineErrorMap.end();
}
