/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "textprocessor.h"

#include <kylin-ai/aiengine.h>

#include <cstdio>
#include <mutex>

#include "config/engineconfiguration.h"
#include "errorcode/errormap.h"
#include "taskmanager/taskrunner.h"
#include "textprocessor.h"

const std::string TextProcessor::objectPath_ =
    "/org/openkylin/aisdk/textprocessor";

std::set<int> TextProcessor::sessionIdSet;

TextProcessor::TextProcessor(
    GDBusConnection &connection,
    ai_engine::AiEnginePluginManager &aiEnginePluginManager)
    : connection_(connection), aiEnginePluginManager_(aiEnginePluginManager) {
    exportSkeleton();
}

TextProcessor::~TextProcessor() { unexportSkeleton(); }

void TextProcessor::stopProcess() {
    NlpEngineMap::iterator iter = nlpEngineMap_.begin();
    while (iter != nlpEngineMap_.end()) {
        iter->second->stopChat();
        ++iter;
    }
    stopped_ = true;
}

void TextProcessor::setSessionChatCallback(gint sessionId) {
    nlpEngineMap_.at(sessionId)->setChatResultCallback(
        [this, sessionId](ai_engine::lm::nlp::ChatResult result) {
            if (stopped_) {
                return;
            }
            std::string interfaceName = "org.openkylin.aisdk.textprocessor" +
                                        std::to_string((int)sessionId);

            GVariantBuilder builder;
            g_variant_builder_init(&builder, G_VARIANT_TYPE_TUPLE);
            g_variant_builder_add(&builder, "s", result.text.c_str());
            g_variant_builder_add(&builder, "i",
                                  getSdkErrorCode(result.error.getCode()));

            std::shared_ptr<TextProcessor> textProcessor =
                static_cast<TextProcessor *>(this)->getSharedObject();
            g_dbus_connection_emit_signal(
                &(textProcessor->connection_),
                "org.openkylin.aisdk.textprocessor",
                "/org/openkylin/aisdk/textprocessor", interfaceName.c_str(),
                "ChatResult", g_variant_builder_end(&builder), nullptr);
        });
}

void TextProcessor::addTaskId(ulong taskId) {
    std::lock_guard<std::mutex> lock(taskIdsMutex_);
    taskIds_.push_back(taskId);
}

void TextProcessor::deleteTaskId(ulong taskId) {
    std::lock_guard<std::mutex> lock(taskIdsMutex_);
    auto iter = std::find(taskIds_.begin(), taskIds_.end(), taskId);
    if (iter != taskIds_.end()) {
        taskIds_.erase(iter);
    }
}

void TextProcessor::deleteRemainingTaskIdsFromTaskRunner() {
    for (const auto taskId : taskIds_) {
        task::TaskRunner::getInstance().deleteTask(taskId);
    }
}

bool TextProcessor::onHandleInitEngine(AisdkTextProcessor *delegate,
                                       GDBusMethodInvocation *invocation,
                                       gchar *engineName, gchar *config,
                                       gpointer userData) {
    auto textProcessor =
        static_cast<TextProcessor *>(userData)->getSharedObject();
    if (!textProcessor) {
        fprintf(stderr, "init engine error: textProcessor is nullptr!\n");
        aisdk_text_processor_complete_init(
            delegate, invocation, 0,
            (int)error_map::nlpEngineErrorMap.at(
                ai_engine::lm::NlpEngineErrorCode::NomoreMemory));
        return true;
    }

    textProcessor->promptTemplate_.reset(
        new config::PromptTemplate(config::promptTemplateConfigFile(engineName),
                                   config::promptFilePath(engineName)));

    int sessionId = textProcessor->generateRandomSessionId();
    std::string engineNameStr(engineName);
    std::string configStr(config);
    ulong taskId = task::generateTaskId();
    textProcessor->addTaskId(taskId);
    task::TaskRunner::getInstance().addTask(
        {taskId, task::TaskType::Initialization,
         [sessionId, delegate, textProcessor, engineNameStr, configStr,
          invocation, taskId]() {
             bool initResult =
                 textProcessor->initEngine(engineNameStr, configStr, sessionId);
             if (!initResult) {
                 aisdk_text_processor_complete_init(
                     delegate, invocation, sessionId,
                     (int)error_map::nlpEngineErrorMap.at(
                         ai_engine::lm::NlpEngineErrorCode::NomoreMemory));
                 textProcessor->deleteTaskId(taskId);
                 return;
             }

             int initModuleErrorCode =
                 textProcessor->initSessionModule(sessionId);
             textProcessor->setSessionChatCallback(sessionId);
             aisdk_text_processor_complete_init(delegate, invocation, sessionId,
                                                initModuleErrorCode);
             textProcessor->deleteTaskId(taskId);
             textProcessor->inited_ = true;
         }});

    return true;
}

bool TextProcessor::onHandleDestroyEngine(AisdkTextProcessor *delegate,
                                          GDBusMethodInvocation *invocation,
                                          gint sessionId, gpointer userData) {
    auto textProcessor =
        static_cast<TextProcessor *>(userData)->getSharedObject();
    if (!textProcessor) {
        fprintf(stderr,
                "nlp destroy session error: textProcessor is nullptr!\n");
        return true;
    }
    auto iter = textProcessor->nlpEngineMap_.find(sessionId);
    if (iter == textProcessor->nlpEngineMap_.end()) {
        fprintf(stderr, "nlp destroy session error: sessionId not exists!\n");
        return true;
    }
    textProcessor->deleteRemainingTaskIdsFromTaskRunner();
    iter->second->stopChat();
    textProcessor->nlpEngineMap_.erase(iter);
    return true;
}

bool TextProcessor::onHandleChat(AisdkTextProcessor *delegate,
                                 GDBusMethodInvocation *invocation,
                                 gchar *question, gint sessionId,
                                 gpointer userData) {
    std::shared_ptr<TextProcessor> textProcessor =
        static_cast<TextProcessor *>(userData)->getSharedObject();
    if (!textProcessor || textProcessor->nlpEngineMap_.find(sessionId) ==
                              textProcessor->nlpEngineMap_.end()) {
        fprintf(stderr,
                "Handle chat error: invalid engine for session id: %d.\n",
                sessionId);
        return true;
    }
    if (!textProcessor->inited_) {
        fprintf(stderr, "Handle chat error: engine not inited.\n");
        return true;
    }

    std::string userQuestion{question};
    if (textProcessor->actorChanged_) {
        userQuestion = textProcessor->prompt_ + ":" + userQuestion;
        textProcessor->actorChanged_ = false;
    }

    ulong taskId = task::generateTaskId();
    textProcessor->addTaskId(taskId);
    task::TaskRunner::getInstance().addTask(
        {task::generateTaskId(), task::TaskType::TextChat,
         [userQuestion, textProcessor, sessionId, taskId]() {
             ai_engine::lm::EngineError engineError;
             auto engine = textProcessor->nlpEngineMap_.at(sessionId);
             bool success = engine->chat(userQuestion, engineError);
             if (!success) {
                 fprintf(stderr, "Text processor chat error: %s!\n",
                         engineError.getMessage().c_str());
             }
             textProcessor->deleteTaskId(taskId);
         }});

    return true;
}

bool TextProcessor::onSetActor(AisdkTextProcessor *delegate,
                               GDBusMethodInvocation *invocation, gint actor,
                               gint sessionId, gpointer userData) {
    TextProcessor *textProcessor = static_cast<TextProcessor *>(userData);
    if (!textProcessor) {
        fprintf(stderr, "set actor error: textProcessor is nullptr!\n");
        return true;
    }

    if (actor == textProcessor->actorId_) {
        return true;
    }

    auto prompt = textProcessor->promptTemplate_->prompt(
        (config::PromptTemplate::PromptId)actor);
    fprintf(stdout, "Actor changed: %s\n", prompt.prompt.c_str());
    textProcessor->actorChanged_ = true;
    textProcessor->actorId_ = actor;
    textProcessor->prompt_ = prompt.prompt;
    return true;
}

bool TextProcessor::onStopChat(AisdkTextProcessor *delegate,
                               GDBusMethodInvocation *invocation,
                               gint sessionId, gpointer userData) {
    TextProcessor *textProcessor = static_cast<TextProcessor *>(userData);
    if (!textProcessor || textProcessor->nlpEngineMap_.find(sessionId) ==
                              textProcessor->nlpEngineMap_.end()) {
        fprintf(stderr,
                "stop chat error: textProcessor or nlpEngine is nullptr!\n");
        return true;
    }

    textProcessor->nlpEngineMap_.at(sessionId)->stopChat();
    return true;
}

bool TextProcessor::onSetContextSize(AisdkTextProcessor *delegate,
                                     GDBusMethodInvocation *invocation,
                                     gint size, gint sessionId,
                                     gpointer userData) {
    TextProcessor *textProcessor = static_cast<TextProcessor *>(userData);
    if (!textProcessor || textProcessor->nlpEngineMap_.find(sessionId) ==
                              textProcessor->nlpEngineMap_.end()) {
        fprintf(
            stderr,
            "set context size error: textProcessor or nlpEngine is nullptr!\n");
        return true;
    }

    textProcessor->nlpEngineMap_.at(sessionId)->setContextSize(size);
    return true;
}

bool TextProcessor::onClearContext(AisdkTextProcessor *delegate,
                                   GDBusMethodInvocation *invocation,
                                   gint sessionId, gpointer userData) {
    TextProcessor *textProcessor = static_cast<TextProcessor *>(userData);
    if (!textProcessor || textProcessor->nlpEngineMap_.find(sessionId) ==
                              textProcessor->nlpEngineMap_.end()) {
        fprintf(
            stderr,
            "clear context error: textProcessor or nlpEngine is nullptr!\n");
        return true;
    }

    textProcessor->nlpEngineMap_.at(sessionId)->clearContext();
    return true;
}

void TextProcessor::exportSkeleton() {
    delegate_ = aisdk_text_processor_skeleton_new();

    g_signal_connect(delegate_, "handle-init", G_CALLBACK(onHandleInitEngine),
                     this);
    g_signal_connect(delegate_, "handle-destroy",
                     G_CALLBACK(onHandleDestroyEngine), this);

    g_signal_connect(delegate_, "handle-chat", G_CALLBACK(onHandleChat), this);

    g_signal_connect(delegate_, "handle-stop-chat", G_CALLBACK(onStopChat),
                     this);

    g_signal_connect(delegate_, "handle-set-actor", G_CALLBACK(onSetActor),
                     this);

    g_signal_connect(delegate_, "handle-set-context",
                     G_CALLBACK(onSetContextSize), this);

    g_signal_connect(delegate_, "handle-clear-context",
                     G_CALLBACK(onClearContext), this);

    GError *error = nullptr;

    isExported_ = g_dbus_interface_skeleton_export(
        G_DBUS_INTERFACE_SKELETON(delegate_), &connection_, objectPath_.c_str(),
        &error);
    if (!isExported_) {
        g_printerr("Error exporting skeleton at address %s: %s\n",
                   objectPath_.c_str(), error->message);
        g_error_free(error);
    }
}

void TextProcessor::unexportSkeleton() {
    if (delegate_ == nullptr) {
        return;
    }

    if (isExported_) {
        g_dbus_interface_skeleton_unexport_from_connection(
            G_DBUS_INTERFACE_SKELETON(delegate_), &connection_);
    }
    sessionIdSet.clear();
    g_object_unref(delegate_);
}

bool TextProcessor::initEngine(const std::string &engineName,
                               const std::string &config, gint sessionId) {
    auto nlpEngine = aiEnginePluginManager_.createNlpEngine(engineName);
    if (!nlpEngine) {
        fprintf(stderr, "initEngine nlpEngine is nullptr !\n");
        return false;
    }

    if (nlpEngine->isCloud()) {
        static_cast<ai_engine::lm::nlp::AbstractCloudNlpEngine *>(
            nlpEngine.get())
            ->setConfig(config);
    }
    task::TaskRunner::getInstance().setMaxConcurrentTasks(
        task::TaskType::TextChat, nlpEngine->maxConcurrentChatTasks());
    nlpEngineMap_[sessionId] = std::move(nlpEngine);
    return true;
}

int TextProcessor::initSessionModule(gint sessionId) {
    ai_engine::lm::EngineError initChatModuleEngineError;
    if (!nlpEngineMap_.at(sessionId)->initChatModule(
            initChatModuleEngineError)) {
        fprintf(stderr,
                "Init chat module engine failed, error code: %d, error "
                "message: %s.\n",
                initChatModuleEngineError.getCode(),
                initChatModuleEngineError.getMessage().c_str());
        return getSdkErrorCode(initChatModuleEngineError.getCode());
    }
    return 0;
}

int TextProcessor::generateRandomSessionId() {
    int sessionId = std::rand();
    while (sessionIdSet.find(sessionId) != sessionIdSet.end()) {
        sessionId = std::rand();
    }
    sessionIdSet.insert(sessionId);
    return sessionId;
}

int TextProcessor::getSdkErrorCode(int engineErrorCode) {
    if (engineErrorCode == -1) {
        return 0;
    } else if (engineErrorCodeExists(engineErrorCode)) {
        return (int)error_map::nlpEngineErrorMap.at(
            (ai_engine::lm::NlpEngineErrorCode)engineErrorCode);
    } else {
        fprintf(stderr,
                "service error messages: speech engine error code is not "
                "exists!\n");
        return 0;
    }
}

bool TextProcessor::engineErrorCodeExists(int errorCode) {
    ai_engine::lm::NlpEngineErrorCode code =
        (ai_engine::lm::NlpEngineErrorCode)errorCode;
    return error_map::nlpEngineErrorMap.find(code) !=
           error_map::nlpEngineErrorMap.end();
}
