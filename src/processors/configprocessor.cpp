/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "processors/configprocessor.h"

const std::string ConfigProcessor::objectPath_ =
    "/com/kylin/AiRuntime/ConfigProcessor";

ConfigProcessor::ConfigProcessor(
    GDBusConnection &connection,
    ai_engine::AiEnginePluginManager &aiEnginePluginManager)
    : connection_(connection), aiEnginePluginManager_(aiEnginePluginManager) {
    exportSkeleton();
}

ConfigProcessor::~ConfigProcessor() { unexportSkeleton(); }

void ConfigProcessor::exportSkeleton() {
    delegate_ = com_kylin_ai_runtime_config_processor_skeleton_new();

    g_signal_connect(delegate_, "handle-get-all-model-info",
                     G_CALLBACK(onHandleGetAllModelInfo), this);

    GError *error = nullptr;

    isExported_ = g_dbus_interface_skeleton_export(
        G_DBUS_INTERFACE_SKELETON(delegate_), &connection_, objectPath_.c_str(),
        &error);
    if (!isExported_) {
        g_printerr("Error exporting skeleton at address %s: %s\n",
                   objectPath_.c_str(), error->message);
        g_error_free(error);
    }
}

void ConfigProcessor::unexportSkeleton() {
    if (delegate_ == nullptr) {
        return;
    }

    if (isExported_) {
        g_dbus_interface_skeleton_unexport_from_connection(
            G_DBUS_INTERFACE_SKELETON(delegate_), &connection_);
    }
    g_object_unref(delegate_);
}

std::string ConfigProcessor::getModelInfo() {
    std::vector<std::string> allEngine = aiEnginePluginManager_.allEngineName();
    std::string allModelInfo;
    std::string nlpModeInfo;
    std::string speechModelInfo;
    std::string visionModelInfo;

    for (const auto &engine : allEngine) {
        std::shared_ptr<ai_engine::lm::nlp::AbstractNlpEngine> nlpEngine =
            aiEnginePluginManager_.createNlpEngine(engine);
        std::shared_ptr<ai_engine::lm::speech::AbstractSpeechEngine>
            speechEngine = aiEnginePluginManager_.createSpeechEngine(engine);
        std::shared_ptr<ai_engine::lm::vision::AbstractVisionEngine>
            visionEngine = aiEnginePluginManager_.createVisionEngine(engine);
        if (nlpEngine) {
            nlpModeInfo =
                static_cast<ai_engine::lm::nlp::AbstractCloudNlpEngine *>(
                    nlpEngine.get())
                    ->modelInfo();
        }
        if (speechEngine) {
            speechModelInfo =
                static_cast<ai_engine::lm::speech::AbstractCloudSpeechEngine *>(
                    speechEngine.get())
                    ->modelInfo();
        }
        if (visionEngine) {
            visionModelInfo =
                static_cast<ai_engine::lm::vision::AbstractCloudVisionEngine *>(
                    visionEngine.get())
                    ->modelInfo();
        }

        allModelInfo += mergeJsonString(
            mergeJsonString(nlpModeInfo, speechModelInfo), visionModelInfo);
    }
    return allModelInfo;
}

std::string ConfigProcessor::mergeJsonString(std::string jsonStr1,
                                             std::string jsonStr2) {
    Json::Value root1, root2;
    Json::CharReaderBuilder builder;
    Json::CharReader *reader = builder.newCharReader();
    std::string errors;

    bool parsingSuccessful1 = reader->parse(
        jsonStr1.c_str(), jsonStr1.c_str() + jsonStr1.size(), &root1, &errors);
    bool parsingSuccessful2 = reader->parse(
        jsonStr2.c_str(), jsonStr2.c_str() + jsonStr2.size(), &root2, &errors);

    if (!parsingSuccessful1 || !parsingSuccessful2) {
        fprintf(stderr, "Failed to parse JSON: %s!", errors.c_str());
        return jsonStr1 + jsonStr2;
    }
    if (!root1.isMember("vendor") || !root2.isMember("vendor")) {
        fprintf(stderr, "Key 'vendor' not found in the Json !");
        return jsonStr1 + jsonStr2;
    }
    if (root1["vendor"] != root2["vendor"]) {
        return jsonStr1 + jsonStr2;
    }
    return mergeJson(root1, root2);
}

std::string ConfigProcessor::mergeJson(Json::Value &json1, Json::Value &json2) {
    Json::Value mergedRoot(Json::objectValue);
    mergedRoot["vendor"] = json1["vendor"];
    json1.removeMember("vendor");
    json2.removeMember("vendor");

    mergeConfig(mergedRoot, json1);
    mergeConfig(mergedRoot, json2);

    return mergedRoot.toStyledString();
}

void ConfigProcessor::mergeConfig(Json::Value &mergeRoot, Json::Value &json) {
    if (!json.isMember("configs")) {
        mergeRoot["configs"].append(json);
    } else if (json["configs"].isArray()) {
        const Json::Value &arrayFromJson1 = json["configs"];
        for (Json::ValueConstIterator it = arrayFromJson1.begin();
             it != arrayFromJson1.end(); ++it) {
            mergeRoot["configs"].append(*it);
        }
    } else {
        mergeRoot["configs"].append(json["configs"]);
    }
}

bool ConfigProcessor::onHandleGetAllModelInfo(
    ComKylinAiRuntimeConfigProcessor *delegate,
    GDBusMethodInvocation *invocation, gpointer userData) {
    ConfigProcessor *configProcessor = static_cast<ConfigProcessor *>(userData);

    com_kylin_ai_runtime_config_processor_complete_get_all_model_info(
        delegate, invocation, configProcessor->getModelInfo().c_str());
    return true;
}
