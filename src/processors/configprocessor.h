/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROCESSORS_CONFIGPROCESSOR_H
#define PROCESSORS_CONFIGPROCESSOR_H

#include <jsoncpp/json/json.h>
#include <kylin-ai/aiengine.h>

#include <string>

#include "configprocessorglue.h"
#include "engine/aienginepluginmanager.h"

class ConfigProcessor {
public:
    ConfigProcessor(GDBusConnection &connection,
                    ai_engine::AiEnginePluginManager &aiEnginePluginManager);

    ~ConfigProcessor();

private:
    void exportSkeleton();
    void unexportSkeleton();
    std::string getModelInfo();
    std::string mergeJsonString(std::string jsonStr1, std::string jsonStr2);
    std::string mergeJson(Json::Value &json1, Json::Value &json2);
    void mergeConfig(Json::Value &mergeRoot, Json::Value &json);

    static bool onHandleGetAllModelInfo(
        ComKylinAiRuntimeConfigProcessor *delegate,
        GDBusMethodInvocation *invocation, gpointer userData);

private:
    bool isExported_ = false;
    ComKylinAiRuntimeConfigProcessor *delegate_ = nullptr;
    GDBusConnection &connection_;

    ai_engine::AiEnginePluginManager &aiEnginePluginManager_;

    static const std::string objectPath_;
};

#endif
