#ifndef SERVICES_VISIONPROCESSOR_H
#define SERVICES_VISIONPROCESSOR_H

#include <atomic>
#include <set>
#include <string>
#include <kylin-ai/aiengine.h>
#include <memory>
#include "visionprocessorglue.h"
#include "engine/aienginepluginmanager.h"

class VisionProcessor : public std::enable_shared_from_this<VisionProcessor> {
public:
    explicit VisionProcessor(
            GDBusConnection &connection,
            ai_engine::AiEnginePluginManager &aiEnginePluginManager);

    ~VisionProcessor();

private:
    void exportSkeleton();
    void connectSignal();
    void unexportSkeleton();
    int generateRandomSessionId();
    int getSdkErrorCode(int engineErrorCode);
    bool engineErrorCodeExists(int errorCode);

    bool initEngine(const std::string &engineName, const std::string &config,
        gint sessionId);
    void setPromptImageCallback(gint sessionId);

    static bool onHandleInitEngine(AisdkVisionProcessor *delegate,
       GDBusMethodInvocation *invocation, gchar *engineName, gchar *config,
       gpointer userData);
    static bool onHandleDestroyEngine(AisdkVisionProcessor *delegate,
                                      GDBusMethodInvocation *invocation,
                                      gint sessionId, gpointer userData);

    static bool onHandleSetPromptImageSize(AisdkVisionProcessor *delegate,
                                           GDBusMethodInvocation *invocation,
                                           gint imageWidth, gint imageHeight,
                                           gint sessionId, gpointer userData);
    static bool onHandleSetPromptImageNumber(AisdkVisionProcessor *delegate,
                                             GDBusMethodInvocation *invocation,
                                             gint imageNumber, gint sessionId,
                                             gpointer userData);
    static bool onHandleSetPromptImageStyle(AisdkVisionProcessor *delegate,
                                            GDBusMethodInvocation *invocation,
                                            gint imageStyle, gint sessionId,
                                            gpointer userData);
    static bool onHandleGetPrompt2ImageSupportedParams(AisdkVisionProcessor *delegate,
                                            GDBusMethodInvocation *invocation,
                                            gint sessionId, gpointer userData);
    static bool onHandlePromptImage(AisdkVisionProcessor *delegate,
                                    GDBusMethodInvocation *invocation,
                                    const gchar *prompt, gint sessionId,
                                    gpointer userData);

private:
    std::shared_ptr<VisionProcessor> getSharedObject() {
        return shared_from_this();
    }

private:
    bool isExported_ = false;
    AisdkVisionProcessor *delegate_ = nullptr;

    GDBusConnection &connection_;

    static const std::string objectPath_;

    ai_engine::AiEnginePluginManager &aiEnginePluginManager_;
    std::atomic_bool stopped_ { false };
    std::string currentPrompt_;
    int actorId_ { -1 };
    bool actorChanged { false };

    static std::set<int> sessionIdSet;
    using VisionEngineMap =
    std::map<gint, std::unique_ptr<ai_engine::lm::vision::AbstractVisionEngine>>;

    VisionEngineMap visionEngineMap_;
};

#endif
