/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SERVICES_TEXTPROCESSOR_H
#define SERVICES_TEXTPROCESSOR_H

#include <kylin-ai/aiengine.h>

#include <atomic>
#include <memory>
#include <mutex>
#include <set>
#include <string>
#include <vector>

#include "config/prompttemplate.h"
#include "engine/aienginepluginmanager.h"
#include "textprocessorglue.h"

class TextProcessor : public std::enable_shared_from_this<TextProcessor> {
public:
    TextProcessor(GDBusConnection &connection,
                  ai_engine::AiEnginePluginManager &aiEnginePluginManager);

    ~TextProcessor();

    void stopProcess();

    static const std::string &getObjectPath() { return objectPath_; }

private:
    void exportSkeleton();
    void unexportSkeleton();
    int generateRandomSessionId();
    int getSdkErrorCode(int engineErrorCode);
    bool engineErrorCodeExists(int errorCode);

    bool initEngine(const std::string &engineName, const std::string &config,
                    gint sessionId);
    int initSessionModule(gint sessionId);
    void setSessionChatCallback(gint sessionId);

    void addTaskId(ulong taskId);
    void deleteTaskId(ulong taskId);
    // 删除剩余的taskid
    void deleteRemainingTaskIdsFromTaskRunner();

    static bool onHandleInitEngine(AisdkTextProcessor *delegate,
                                   GDBusMethodInvocation *invocation,
                                   gchar *engineName, gchar *config,
                                   gpointer userData);
    static bool onHandleDestroyEngine(AisdkTextProcessor *delegate,
                                      GDBusMethodInvocation *invocation,
                                      gint sessionId, gpointer userData);

    static bool onHandleChat(AisdkTextProcessor *delegate,
                             GDBusMethodInvocation *invocation, gchar *question,
                             gint sessionId, gpointer userData);

    static bool onSetActor(AisdkTextProcessor *delegate,
                           GDBusMethodInvocation *invocation, gint actor,
                           gint sessionId, gpointer userData);

    static bool onStopChat(AisdkTextProcessor *delegate,
                           GDBusMethodInvocation *invocation, gint sessionId,
                           gpointer userData);

    static bool onSetContextSize(AisdkTextProcessor *delegate,
                                 GDBusMethodInvocation *invocation, gint size,
                                 gint sessionId, gpointer userData);

    static bool onClearContext(AisdkTextProcessor *delegate,
                               GDBusMethodInvocation *invocation,
                               gint sessionId, gpointer userData);

private:
    std::shared_ptr<TextProcessor> getSharedObject() {
        return shared_from_this();
    }

private:
    bool isExported_ = false;
    AisdkTextProcessor *delegate_ = nullptr;
    GDBusConnection &connection_;
    ai_engine::AiEnginePluginManager &aiEnginePluginManager_;
    std::unique_ptr<config::PromptTemplate> promptTemplate_;
    std::atomic_bool inited_{false};
    int actorId_{-1};
    std::string prompt_;
    bool actorChanged_{false};
    std::atomic_bool stopped_{false};
    std::mutex taskIdsMutex_;
    std::vector<ulong> taskIds_;

    static const std::string objectPath_;

    static std::set<int> sessionIdSet;
    using NlpEngineMap =
        std::map<gint, std::shared_ptr<ai_engine::lm::nlp::AbstractNlpEngine>>;

    NlpEngineMap nlpEngineMap_;
};

#endif
