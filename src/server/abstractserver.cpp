/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "abstractserver.h"

#include <sys/stat.h>
#include <sys/types.h>

#include <iostream>

#define MODE 0755

const std::string AbstractServer::socketFilePath_ =
    "/tmp/.kylin-ai-runtime-unix/" + std::to_string(getuid()) + "/";

AbstractServer::AbstractServer(const std::string &filePath)
    : filePath_(filePath) {
    checkSocketFilePath();
    startServer();
}

AbstractServer::~AbstractServer() { stopServer(); }

void AbstractServer::startServer() {
    auto *guid = g_dbus_generate_guid();
    GError *error = nullptr;

    std::string unixPath = "unix:path=" + filePath_;
    server_ = g_dbus_server_new_sync(unixPath.c_str(),         /* address */
                                     G_DBUS_SERVER_FLAGS_NONE, /* flags */
                                     guid,                     /* guid */
                                     nullptr, /* GDBusAuthObserver */
                                     nullptr, /* GCancellable */
                                     &error);
    if (server_ == nullptr) {
        g_printerr("Error creating server at address %s: %s\n",
                   unixPath.c_str(), error->message);
        g_error_free(error);
        return;
    }

    g_dbus_server_start(server_);

    g_free(guid);

    g_signal_connect(server_, "new-connection", G_CALLBACK(onNewConnection),
                     this);
}

void AbstractServer::stopServer() {
    if (server_ == nullptr) {
        return;
    }

    g_dbus_server_stop(server_);
    g_object_unref(server_);
}

void AbstractServer::checkSocketFilePath() {
    if (pathExist(socketFilePath_)) {
        unlink(filePath_.c_str());
        return;
    }

    const char *socketPath = socketFilePath_.c_str();
    int len = strlen(socketPath);

    for (int i = 0; i < len; ++i) {
        if (socketPath[i] != '/') {
            continue;
        }

        char subdir[i + 1];
        strncpy(subdir, socketPath, i + 1);
        subdir[i + 1] = '\0';
        if (pathExist(subdir)) {
            continue;
        }

        if (mkdir(subdir, MODE) != 0) {
            fprintf(stderr, "Failed to create config server directory: %s\n",
                    subdir);
            return;
        }
    }
}

bool AbstractServer::pathExist(const std::string &path) {
    struct stat st;
    if (stat(path.c_str(), &st) == 0 && S_ISDIR(st.st_mode)) {
        return true;
    }
    return false;
}

gboolean AbstractServer::onNewConnection(GDBusServer * /* server */,
                                         GDBusConnection *connection,
                                         gpointer userData) {
    auto *server = static_cast<AbstractServer *>(userData);

    server->handleNewConnection(connection);
    server->closedHandlerId_ = g_signal_connect(
        connection, "closed", G_CALLBACK(server->onConnectionClosed), server);
    return true;
}

void AbstractServer::onConnectionClosed(GDBusConnection *connection,
                                        gboolean /* remotePeerVanished */,
                                        GError * /* error */,
                                        gpointer userData) {
    std::cout << "Connection closed." << std::endl;
    auto *server = static_cast<AbstractServer *>(userData);

    if (server->closedHandlerId_ > 0) {
        g_signal_handler_disconnect(connection, server->closedHandlerId_);
        server->closedHandlerId_ = 0;
    }

    g_object_unref(connection);
}
