/*
 * Copyright 2024 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SERVER_CONFIGSERVER_H
#define SERVER_CONFIGSERVER_H

#include <memory>
#include <string>

#include "abstractserver.h"
#include "engine/aienginepluginmanager.h"
#include "processors/configprocessor.h"

class ConfigServer : public AbstractServer {
public:
    static ConfigServer &getInstance();

    ConfigServer();
    ~ConfigServer() override;

protected:
    void handleNewConnection(GDBusConnection *connection) override;
    void handleDisconnection() override;

private:
    void checkFilePath();

private:
    static const std::string filePath_;

    ai_engine::AiEnginePluginManager aiEnginePluginManager_;

    std::shared_ptr<ConfigProcessor> configProcessor_;
};

#endif
