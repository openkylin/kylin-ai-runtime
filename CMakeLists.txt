cmake_minimum_required(VERSION 3.5)

project(kylin-ai-runtime LANGUAGES CXX C)

set (CMAKE_EXPORT_COMPILE_COMMANDS ON)
set (CMAKE_SKIP_RPATH ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(PkgConfig REQUIRED)
find_package(KylinAiEngine REQUIRED)
find_package(KylinAiProto REQUIRED)
find_package(OpenSSL REQUIRED)
include_directories(${OPENSSL_INCLUDE_DIR})
pkg_check_modules(GIO REQUIRED gio-unix-2.0)
pkg_check_modules(PORTAUDIO REQUIRED portaudio-2.0)
kylin_ai_generate_gdbus_proto_code(PROTO_FILES textprocessor speechprocessor visionprocessor keywordrecognizer configprocessor)

include_directories(${GIO_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/third-party)
include(GNUInstallDirs)
include(cmake/engine-plugin-path-defs.cmake)
include_directories(
    ./src
)

set(CPR_THREADPOOL_SOURCES
    src/threadpool/singleton.h
    src/threadpool/async.h
    src/threadpool/async.cpp
    src/threadpool/threadpool.h
    src/threadpool/threadpool.cpp
)

add_executable(kylin-ai-runtime
    main.cpp
    src/server/server.cpp
    src/server/server.h
    src/server/abstractserver.cpp
    src/server/abstractserver.h
    src/server/configserver.cpp
    src/server/configserver.h
    src/server/servicemanager.cpp
    src/server/servicemanager.h
    src/engine/aienginepluginmanager.h
    src/engine/aienginepluginmanager.cpp
    src/engine/library.h
    src/engine/library.cpp
    src/processors/textprocessor.cpp
    src/processors/textprocessor.h
    src/processors/speechprocessor.cpp
    src/processors/speechprocessor.h
    src/processors/visionprocessor.cpp
    src/processors/visionprocessor.h
    src/processors/configprocessor.cpp
    src/processors/configprocessor.h
    # src/services/keywordrecognizer.cpp
    # src/services/keywordrecognizer.h
    src/config/engineconfiguration.h
    src/config/engineconfiguration.cpp
    src/config/prompttemplate.h
    src/config/prompttemplate.cpp
    src/microphone/microphone.h
    src/microphone/microphone.cpp
    src/taskmanager/taskrunner.h
    src/taskmanager/taskrunner.cpp
    ${CPR_THREADPOOL_SOURCES}
    ${PROTO_FILES}
    src/errorcode/sdkerror.h
    src/errorcode/errormap.h
)

target_link_libraries(
    kylin-ai-runtime
    pthread
    jsoncpp
    crypto
    dl
    ${GIO_LIBRARIES}
    ${PORTAUDIO_LIBRARIES}
    ${OPENSSL_LIBRARIES}
)

if (ENABLE_TEST)
    enable_testing()
    add_subdirectory(test)
endif ()

include(CMakePackageConfigHelpers)

install(TARGETS kylin-ai-runtime DESTINATION /usr/bin)
install(DIRECTORY configs/etc/ DESTINATION /etc)
install(DIRECTORY configs/lib/ DESTINATION /lib)
